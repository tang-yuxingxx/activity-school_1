/**  
 * 冒泡排序函数  
 * 该函数将给定的数组进行冒泡排序，使其从小到大有序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    // 外层循环控制排序的轮数  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环控制每轮比较的次数  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果前一个元素大于后一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
    // 当所有循环结束后，数组a已经变得有序  
} //end
