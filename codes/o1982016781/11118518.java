public static void bubbleSort(int [] b, int n) {
    for (int i=0 ; i<n-1 ;i++){
        for (int j=0 ; j<n-1-i ;j++){
            if (b[j]>b[j+1]) {
                int temp=b[j];
                b[j]=b[j+1];
                b[j+1]=temp;
            }
       }
    }
}