/**  
 * 冒泡排序函数  
 * 通过比较相邻的元素来工作，如果它们的顺序错误就把它们交换过来。  
 * 遍历数组的工作是重复地进行直到没有再需要交换，也就是说该数组已经排序完成。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 标记变量，用于检测在一次遍历中是否发生了交换  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                // 交换元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记发生了交换  
                swapped = true;  
            }  
        }  
        // 如果在一次遍历中没有发生交换，说明数组已经有序，可以提前结束  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
