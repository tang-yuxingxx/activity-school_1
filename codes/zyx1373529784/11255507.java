/**
 * 冒泡排序函数
 * aa bb cc
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int [] a, int n){
    // 你的代码，使无序数组 a 变得有序
    int temp;
    for (int j = 0; j < n - 1; n--) {
        for (int i = 0; i < n - 1; i++) {
            if (a[i] > a[i + 1]) {
                temp = a[i + 1];
		a[i + 1] = a[i];
		a[i] = temp;
	    }
	}
    }
} //end
