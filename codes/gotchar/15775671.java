/**  
 * 冒泡排序函数  
 * 通过相邻元素两两比较并交换，使得每一轮比较后最大（或最小）的元素能够移动到数组的末尾  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) { // 外层循环控制排序趟数  
        for (int j = 0; j < n - 1 - i; j++) { // 内层循环控制每一趟排序多少次  
            if (a[j] > a[j + 1]) { // 如果当前元素大于下一个元素，则交换它们的位置  
                // 交换 a[j] 和 a[j + 1]  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
