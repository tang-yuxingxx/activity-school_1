/**
 * 冒泡排序函数
 * 该算法通过重复遍历待排序的数组，比较相邻元素并交换位置，直到数组有序。
 * @param a 待排序的数组
 * @param n 待排序的数组长度
 */
public static void bubbleSort(int[] a, int n) {
    // 外层循环控制遍历次数
    for (int i = 0; i < n - 1; i++) {
        // 内层循环负责进行相邻元素的比较和交换
        for (int j = 0; j < n - i - 1; j++) {
            // 如果当前元素大于后一个元素，则交换它们的位置
            if (a[j] > a[j + 1]) {
                // 交换操作
                int temp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = temp;
            }
        }
    }
    // 排序完成后，数组 a 将变得有序
}
