/**  
 * 冒泡排序函数  
 * 该函数使用冒泡排序算法对给定的数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 标志位，表示这一趟排序是否有交换，优化冒泡排序性能  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 发生了交换，将标志位设为true  
                swapped = true;  
            }  
        }  
        // 如果没有发生交换，说明数组已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
