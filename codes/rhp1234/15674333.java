/**  
 * 冒泡排序函数  
 * 该函数通过冒泡排序算法对整数数组进行升序排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    for (int i = 0; i < n - 1; i++) {  
        // 用于标记是否发生交换，以优化排序过程  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            if (a[j] > a[j + 1]) {  
                // 交换两个元素  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 标记发生了交换  
                swapped = true;  
            }  
        }  
        // 如果没有发生交换，则数组已经有序，可以提前结束排序  
        if (!swapped) {  
            break;  
        }  
    }  
} //end
