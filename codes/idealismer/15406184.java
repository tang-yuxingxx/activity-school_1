/**  
 * 冒泡排序函数  
 * 通过比较相邻元素并交换它们（如果需要），将数组a排序为升序。  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int[] a, int n) {  
    for (int i = 0; i < n - 1; i++) {  
        // 创建一个标志位，用于检测数组是否已排序  
        boolean swapped = false;  
        for (int j = 0; j < n - i - 1; j++) {  
            // 如果当前元素大于下一个元素，则交换它们  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
                // 交换后，标志位设为true  
                swapped = true;  
            }  
        }  
        // 如果内层循环没有交换任何元素，说明数组已排序完成  
        if (!swapped) {  
            break;  
        }  
    }  
} // end bubbleSort
