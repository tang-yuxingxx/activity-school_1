/**  
 * 冒泡排序函数  
 * 对数组进行冒泡排序，从小到大排序  
 * @param a 待排序的数组  
 * @param n 待排序的数组长度  
 */  
public static void bubbleSort(int [] a, int n){  
    // 外层循环控制所有元素是否排序完成  
    for (int i = 0; i < n - 1; i++) {  
        // 内层循环控制相邻元素两两比较  
        for (int j = 0; j < n - 1 - i; j++) {  
            // 如果前一个元素大于后一个元素，则交换他们的位置  
            if (a[j] > a[j + 1]) {  
                int temp = a[j];  
                a[j] = a[j + 1];  
                a[j + 1] = temp;  
            }  
        }  
    }  
} //end
